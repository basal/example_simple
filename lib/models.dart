import 'package:basal/basal.dart';

class User extends Model {
  final String firstName;
  final String lastName;

  User(this.firstName, this.lastName);
}

